# ☆ 使い方 ☆彡

## 1. node.jsをインストールします。

githubからnvmスクリプトをcloneします。

    git clone git://github.com/creationix/nvm.git ~/.nvm

nvm.shを読み込みます。

     source ~/.nvm/nvm.sh

nodejsをインストールします。

    nvm install v0.10.13


指定したバージョンのnodejsを使用します。

    nvm use v0.10.13


~/.bashrc, ~/.zshrc等にnvmパスの読み込みを追記します。

    if [ -f ~/.nvm/nvm.sh ]; then
      . ~/.nvm/nvm.sh
      nvm use v0.10.13
    fi


## 2. grunt-cliをglobalにインストールします。

    npm install -g grunt-cli


* npm・・・nodejsのパッケージマネージャ
* -gオプション・・・このプロジェクトのみで有効とするのではなく、マシン全体にインストール


## 3. 依存モジュールをインストールします。

    npm install
    ※ package.jsonにdevDependenciesとして定義されたnodejsモジュールがインストールされます。

  
--

# ☆ テンプレートファイルのプリコンパイル ☆彡

    hogan.js用のテンプレートファイルは、実行時にコンパイルすることもできますが、
    できれば事前にコンパイルして置きたいと思うのが人情です。
    そこで、grunt-hoganモジュールを利用してテンプレートファイルをプリコンパイルします。

## 1. grunt-hoganについて

https://github.com/automatonic/grunt-hogan

## 2. gruntコマンドを実行します。

    grunt hogan

* package.jsonにhoganという名前のタスクを定義しています。
* templates/ディレクトリからテンプレートファイルを読み取り、　builds/ディレクトリにコンパイル済みファイルを置くように設定しています。

