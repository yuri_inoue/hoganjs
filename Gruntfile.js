module.exports = function( grunt ) {

  var pkg = grunt.file.readJSON( 'package.json' );

  grunt.initConfig({
    hogan: {
      modules: {
        templates: './templates/modules/*.html',
        output: './builds/modules.js',
        binderName: 'hulk'
      },
      index: {
        templates: [
                      './templates/modules/*.html',
                      './templates/index.html'
                   ],
        output: './builds/index.js',
        binderName: 'hulk'
      }
    },
    cssmin: {
      compress: {
        files: {
          './min.css': [ 'css/style.css' ]
        }
      }
    },
    watch: {
      files: [ 'css/*.css' ],
      tasks: [ 'cssmin' ]
    }
  });

  grunt.loadTasks( 'tasks' );

  var taskName;
  for ( taskName in pkg.devDependencies ) {
    if ( taskName.substring(0, 6) == 'grunt-' ) {
      grunt.loadNpmTasks( taskName );
    }
  }
  grunt.registerTask( 'default', [ 'hogan', 'watch' ] );
};

