define( [
  'jquery',
  'underscore',
  'hogan',
  'backbone',
  'views/index'
], function( $, _, h, backbone, IndexView ) {

  var AppRouter = Backbone.Router.extend( {
    routes: {
      // default
      '*actions': 'default'
    },
    default: function() {
       console.log('default');
       var index = new IndexView();
       index.render();
    }
  } );


  var initialize = function() {

    var router = new AppRouter;
    Backbone.$ = $;
    Backbone.history.start();

  };


  return {
    initialize: initialize
  };
} );
