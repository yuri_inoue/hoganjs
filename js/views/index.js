define( [
  'jquery',
  'hogan',
  'backbone',
  "$TEMPLATES/index",
], function( $, h, backbone, index ) {

  var IndexView = Backbone.View.extend( {
    el: $( '#page' ),
    render: function() {
      var data = {
        members_num: 10,
        photos_num: 120,
        past_days: 5
      };

      var header = templates.header.render();
      this.$el.append( header );

      var main = templates.index.render( data );
      this.$el.append( main );

      var sidebar = templates.sidebar.render();
      this.$el.append( sidebar );
    }
  } );

  return IndexView;
} );
