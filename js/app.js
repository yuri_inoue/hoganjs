define( [
  'jquery',
  'underscore',
  'hogan',
  'backbone',
  'router'
], function( $, _, h, backbone, router ) {
  var initialize = function() {
    router.initialize();
  };

  return {
    initialize: initialize
  };
} );
