require.config( {
  paths: {
    "jquery": "lib/jquery-2.0.3",
    "backbone": "lib/backbone",
    "hogan": "lib/hogan-2.0.0",
    "underscore": "lib/underscore",
    '$TEMPLATES': '../builds'
  }
} );


require( [
  'app'
], function( app ) {
  app.initialize();
} );

